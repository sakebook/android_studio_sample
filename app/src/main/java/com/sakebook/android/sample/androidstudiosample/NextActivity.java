package com.sakebook.android.sample.androidstudiosample;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;

/**
 * Created by sakemoto on 2014/07/02.
 */
public class NextActivity extends Activity{

    private final static String INTENT_DATA = "data";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ((Button)findViewById(R.id.button2)).setText(getData(getIntent()));
    }

    private String getData (Intent intent) {
        if (intent == null) {
            return "";
        }
        String data = intent.getStringExtra(INTENT_DATA);
        if (TextUtils.isEmpty(data)) {
            return "";
        }
        return data;
    }


}
